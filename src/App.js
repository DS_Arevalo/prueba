import React, { useState, useEffect } from "react";
import Header from "./components/Header/Header"
import Body from "./components/Body/Body"
import petition_get from "./petitions/petition_get";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import CircularProgress from '@material-ui/core/CircularProgress';
import './App.css';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
      marginRight: theme.spacing(2),
      with: "100%",
      display: "flex",
      justifyContent: "center",
      paddingBottom: "1em",
      color: "white"
    },
  },
  spinner: {
    width: "100%",
    justifyContent: "center",
    paddingTop: "20%",
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  pagination: {
    '& ul > li > button': {
      backgroundColor: 'transparent',
      color: '#919191',
    },
    '& .Mui-selected': {
      // backgroundColor: 'transparent',
      color: 'white',
    },
  }
}));


function App() {
  const [data, setData] = useState(null)
  const [pages, setPages] = useState(null)
  const [actualPage, setActualPage] = useState(null)
  const [spinner, setSpinner] = useState(true)
  const [filterText, setFilterText] = useState("")

  const classes = useStyles();

  useEffect(() => {
    getMovies("?filter=now_playing")
  }, [])

  useEffect(() => {
    console.log(filterText);
    setSpinner(true);
    if (filterText === "") {
      getMovies("?filter=now_playing")
    } else {
      getMovies(filterText)

    }
  }, [filterText])

  const getMovies = (filters) => {
    petition_get("movies", filters)
      .then((res) => {
        console.log(res);
        setData(res.data.results)
        setActualPage(res.data.page)
        setPages(res.data.total_pages)
        setSpinner(false);
      })
      .catch((err) => {
        console.log(err)
        setSpinner(false);

      })
  }
  const handleChange = (event, value) => {
    setSpinner(true);
    let filterPage =filterText != ""? filterText+"&page=" + value:"?page=" + value
    getMovies(filterPage);
  };

  return (
    <div className="App">
      <Header setFilterText={setFilterText} />
      <br></br>
      {spinner ? <div className={classes.spinner}>
        <CircularProgress color="primary" />
      </div> : <>{data && <Body data={data} />}

        <div className={classes.root}>
          {data && <Pagination className={classes.pagination} count={pages} page={actualPage} size="large" color="primary" onChange={handleChange} />}
        </div>
      </>
      }
    </div>

  );
}

export default App;
