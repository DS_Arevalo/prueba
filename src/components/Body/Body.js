import React from "react";
import Cards from "../Card/Card";
import './Body.css';


export default function Body({ data }) {

    return (
        <>
            <div className="Body">
                {data.map((element, i) => <Cards key={i} info={element} />)}
            </div>
        </>
    )
}