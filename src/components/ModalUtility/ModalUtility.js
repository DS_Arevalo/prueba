import React from 'react';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Rating from '@material-ui/lab/Rating';
import noImage from '../../assets/notFound.jpg';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'scroll',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    width: "70%"

  },
  infoMovie: {
    backgroundSize: "cover",
    width: "100%",
    height: "500px",
    display: "flex",
    alignItems: "flex-end",
  }
}));

const StyledRating = withStyles({
  iconEmpty:{
    color: 'white',
  },
  iconFilled: {
    color: '#yellow',
  },
  iconHover: {
    color: '#000',
  },
})(Rating);

export default function ModalUtility({ modal, setModal, info }) {
  const classes = useStyles();
  const json = require("../../config.json");
  const urlImgs = json.urlImgs;

  const handleClose = () => {
    setModal(false);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={modal}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={modal}>
          <div className={classes.paper}>
            <div className={classes.infoMovie} style={{ backgroundImage:info.backdrop_path? `url("${urlImgs}${info.backdrop_path}")`:`url("${noImage}")`, backgroundSize: "cover" }}>
              {/* <image style={{ width: "200px", height: "500px" }} src={urlImgs + info.poster_path} />
            <image style={{ width: "200px", height: "500px" }} src={urlImgs + info.backdrop_path} /> */}
              <div style={{ background: "rgba(0,0,0,0.7)", bottom: 0,width:"100%" }}>
                <h2 style={{ paddingTop: 10, paddingLeft: 10, margin: 0, color: "white" }}>{info.title + " - (" + info.original_language + ")"}  <StyledRating name="read-only" style={{top:4}} value={(info.vote_average / 2)} precision={0.01} readOnly /></h2>
                <p style={{ paddingLeft: 10, paddingBottom: 5, margin: 0, color: "white", fontSize: "0.8em" }}>{"Release date: " + info.release_date.replaceAll("-", "/")}</p>
                {/* <p style={{ paddingLeft: 10, paddingBottom: 5, margin: 0, color: "white", fontSize: "0.8em" }}>{"qualification: " + info.vote_average + "/10  - " + info.vote_count + " votes"}</p> */}
                <p style={{ paddingLeft: 10, paddingBottom: 5, margin: 0, color: "white", fontSize: "0.8em" }}>{"Popularity: " + info.popularity}</p>
                <p style={{ paddingLeft: 10, paddingRight: 10, margin: 0, color: "white" }}>{"Overview:"}</p>
                <p style={{ paddingLeft: 10, paddingBottom: 10, paddingRight: 10, margin: 0, color: "white" }}>{info.overview}</p>
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
    </div >
  );
}