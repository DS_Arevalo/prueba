import React, { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import ModalUtility from '../ModalUtility/ModalUtility';
import noImage from '../../assets/notFound.jpg';

const useStyles = makeStyles({
  root: {
    maxWidth: 250,
    maxHeight:400,
    margin: 5
  },
});

export default function ImgMediaCard({ info }) {
  const[modal,setModal] = useState(false);
  const classes = useStyles();
  const json = require("../../config.json");
  const urlImgs = json.urlImgs;


  return (
    <>
      {modal && <ModalUtility modal={modal} setModal={setModal} info={info}/>}
      <Card className={classes.root}>
        <CardActionArea onClick={() => {setModal(true) }}>
          <CardMedia
            component="img"
            alt={info.title}
            height="400"
            image={info.poster_path? urlImgs + info.poster_path:noImage}
            title={info.title}
          />
        </CardActionArea>
      </Card>
    </>
  );
}