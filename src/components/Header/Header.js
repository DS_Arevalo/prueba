import React from "react";
import {withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Schedule from '@material-ui/icons/Schedule';
import Star from '@material-ui/icons/Star';
import Visibility from '@material-ui/icons/Visibility';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import './Header.css';

const CssTextField = withStyles({
    root: {
        '& label ': {
            color: 'white',
        },
        '& input ': {
            color: 'white',
            borderBottomColor: "white"
        },
        '& label.MuiInput-underline': {
            borderBottomColor: "white"
        },
        '& label.Mui-focused': {
            color: 'white',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'white',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'red',
            },
            '&:hover fieldset': {
                borderColor: 'yellow',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'green',
            },
        },
    },
})(TextField);


export default function Header({ setFilterText }) {
    const [alignment, setAlignment] = React.useState('Recent');

    const handleChange = (event, newAlignment) => {
        console.log(newAlignment);
        setAlignment(newAlignment);
        switch (newAlignment) {
            case "Recent":
                setFilterText("?filter=now_playing")
                break;
            case "Rated":
                setFilterText("?filter=top_rated")
                break;
            case "Popularity":
                setFilterText("?filter=popular")
                break;
            default:
                setFilterText("?filter=null")
                break;
        }
    };
    const changeFilterText = (value) => {

        if (value === "") {
            setAlignment("Recent");
            setFilterText("?filter=now_playing")
        } else {
            setAlignment("null");
            setFilterText("?name=" + value)
        }
    }

    return (
        <>
            <div className="Header">
                <div style={{ display: "flex", marginLeft: 20, padding: 15 }}>
                    <CssTextField style={{ display: "relative" }} id="standard-basic" label="Search" onChange={(e) => { changeFilterText(e.target.value) }} />
                    <ToggleButtonGroup style={{ marginTop: 10, marginLeft: 10 }} size="small" value={alignment} exclusive onChange={handleChange}>
                        <ToggleButton style={{ color: "white" }} value="Recent">
                            <Schedule fontSize="small" color={"white"} />
                        </ToggleButton>
                        <ToggleButton style={{ color: "white" }} value="Rated">
                            <Star fontSize="Star" />
                        </ToggleButton>
                        <ToggleButton style={{ color: "white" }} value="Popularity">
                            <Visibility fontSize="small" />
                        </ToggleButton>
                        <ToggleButton style={{ color: "white", visibility: "hidden" }} value="null">
                            {/* <Visibility fontSize="small" /> */}
                        </ToggleButton>
                    </ToggleButtonGroup>
                </div>
            </div>
        </>
    )
}