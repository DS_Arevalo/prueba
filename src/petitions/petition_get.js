import axios from "axios";


export default function petition_get(key,value){
    const json = require("../config.json");
    const urlBase = json.urlBase;
    let url;
    switch (key) {
        case "movies":
            url = `${urlBase}/api/movies${value? value:''}`;
            break;
    
        default:
            break;
    }

    return axios.get(url);
};